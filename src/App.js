import React from 'react';
import './App.css';
import Header from './Components/Header/Header';
import RecommendedVideos from './Components/RecommendedVideos/RecommendedVideos';
import Sidebar from './Components/Sidebar/Sidebar';
import {
  BrowserRouter as Router,
  Switch,
  Route} from "react-router-dom";
import SearchPage from './Components/Search/SearchPage';
function App() {
  return (
    <div className="app">
      <Router>
        {/* Header */}
     <Header/>
       <Switch>
       <Route path="/search/:searchTerm">
            <div className="main__page">
              <Sidebar />
              <SearchPage />
            </div>
          </Route>
         <Route path="/">
         <div className="main__page">
       {/* sidebar */}
     <Sidebar/>
     {/* recommerded */}
     <RecommendedVideos />
     </div>
         </Route>
        </Switch> 
      </Router>
     
     
    </div>
  );
}

export default App;
