import React, { useState } from 'react'
import "./Header.css";
import MenuSharpIcon from '@material-ui/icons/MenuSharp';
import SearchSharpIcon from '@material-ui/icons/SearchSharp';
import VideoCallSharpIcon from '@material-ui/icons/VideoCallSharp';
import AppsSharpIcon from '@material-ui/icons/AppsSharp';
import NotificationsSharpIcon from '@material-ui/icons/NotificationsSharp';
import { Avatar } from '@material-ui/core';
import { Link } from 'react-router-dom';

const Header = ()=> {
  const [inputSearch, setInputSearch] = useState('');
    return (
        <div className="header">
         <div className="header__left">
          <MenuSharpIcon/>
          <Link to="/">
          <img
            className="header__logo"
            src="https://upload.wikimedia.org/wikipedia/commons/e/e1/Logo_of_YouTube_%282015-2017%29.svg"
            alt="YouTube Logo"
          />
          </Link>
          </div>
          <div className="header__input">
          <input type="search" value={inputSearch} onChange={(e) => setInputSearch(e.target.value)}/>
          <Link to={`/search/${inputSearch}`}>
           <SearchSharpIcon/>
           </Link>
           </div>
           <div className="header__icons">
           <VideoCallSharpIcon className="header__icon"/>
           <AppsSharpIcon className="header__icon"/>
           <NotificationsSharpIcon className="header__icon"/>
           <Avatar alt="Remy Sharp" src="https://secure.gravatar.com/avatar/3745990a425cf002fc133e48d120a397?s=100&d=mm&r=g" />
        </div>
        </div>
    )
}

export default Header
